
import 'package:cravings/item_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return DefaultTabController(
      length: 10,
      child: Scaffold(
        backgroundColor: Color.fromARGB(127, 197, 212, 243),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(top: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                 
                  
                ],
                ),
                ),
                SizedBox(height: 30),
                Padding(padding: EdgeInsets.symmetric(horizontal: 15),
                child: Text(
                  "Order Now",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                ),
                SizedBox(height: 5),
          Padding(padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              "Categories",
              style: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
                SizedBox(height: 30),
                TabBar(
                  isScrollable: true,
                  indicator: BoxDecoration(),
                  labelStyle: TextStyle(fontSize: 22),
                  labelPadding: EdgeInsets.symmetric(horizontal: 30),
                  tabs: [
                    Tab(text: "Burger"),
                    Tab(text: "Pizza"),
                    Tab(text: "Cheese"),
                    Tab(text: "Pasta"),
                  ],
                ),
                Flexible(
                  flex: 1,
                    child: TabBarView(
                      children: [
                        ItemsWidget(),
                        ItemsWidget(),
                        ItemsWidget(),
                        ItemsWidget(),
                ],)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
