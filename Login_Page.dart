import 'package:cravings/home_page.dart';
import 'package:flutter/material.dart';


class LoginPage extends StatelessWidget{
  bool isFinished = false;
  void navigateNextPage(BuildContext ctx)
  {
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_){
      return HomePage();
    }));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(127, 197, 212, 243),
      appBar: AppBar(
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
             Text(
              'Cravings',
              style: TextStyle(
                  fontSize: 50,
                  color: Colors.black,
                  fontWeight: FontWeight.bold
              ),
            ),
              Text(
              'Satisfied',
              style: TextStyle(
                  fontSize: 10,
                  color: Colors.black,
                  fontWeight: FontWeight.bold
              ),
            ),
             Padding(padding: const EdgeInsets.symmetric(vertical: 50)),

            Text(
              'Login',
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.black,
            
              ),
            ),
            Padding(padding: const EdgeInsets.symmetric(vertical: 10)),

    
            Padding(padding: const EdgeInsets.symmetric(vertical: 10)),

            Form(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'Enter Email',
                        prefixIcon: Icon(Icons.email),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (String value) {

                      },
                      validator: (value) {
                        return value!.isEmpty ? 'Please enter Email' : null;
                      },
                    ),
                  ),

                ],
              ),
            ),
            Padding(padding: const EdgeInsets.symmetric(vertical: 10)),

            
      
            Form(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    child: TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                        labelText: 'Enter Password',
                        prefixIcon: Icon(Icons.password),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (String value) {

                      },
                      validator: (value) {
                        return value!.isEmpty ? 'Please enter Password' : null;
                      },
                    ),
                  ),
                ],
              ),
            ),

            Padding(padding: const EdgeInsets.symmetric(vertical: 10)),
            ElevatedButton(
              onPressed: (){
                navigateNextPage(context);

              },
              child: const Text('Login',
                style:  TextStyle(fontWeight: FontWeight.bold),
              ),
            ),


          ],
        ),
      ),

    );
  }
}