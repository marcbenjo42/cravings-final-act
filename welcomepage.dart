
import 'package:cravings/login_Signup_Page.dart';
import 'package:flutter/material.dart';


class welcomepage extends StatelessWidget{
  void navigateNextPage(BuildContext ctx)
  {
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_){
      return login_SignupPage();
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(127, 197, 212, 243),
      appBar: AppBar(
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
                Text(
              'Welcome ',
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                
              ),
            ),
             Text(
              'to ',
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                
              ),
            ), 
            Text(
              'CRAVINGS',
              style: TextStyle(
                  fontSize: 70,
                  color: Colors.black,
                  fontWeight: FontWeight.bold
              ),
            ),
            Padding(padding: const EdgeInsets.symmetric(vertical: 90)),
        
            Text(
              "Let's fulfill our cravings",
              style: TextStyle(
                  fontSize: 20,
                  color: const Color.fromARGB(108, 0, 0, 0),

              ),
            ),


            Padding(padding: const EdgeInsets.symmetric(vertical: 10)),
            ElevatedButton(
              onPressed: (){
                navigateNextPage(context);

              },
              child: const Text("Let's Start",
                style:  TextStyle(fontWeight: FontWeight.bold),
              ),
            ),


          ],
        ),

      ),

    );
  }
  }
  
  